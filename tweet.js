const axios = require('axios');
const twit = require('twit');
global.media_id = null;

const twitterConfig = {
    consumer_key: process.env.CONSUMER_KEY,
    consumer_secret: process.env.CONSUMER_SECRET,
    access_token: process.env.ACCESS_TOKEN_KEY,
    access_token_secret: process.env.ACCESS_TOKEN_SECRET,
};

const twitterClient = new twit(twitterConfig);

// Tweet a text-based status
async function tweet(data)
{
    const tweet_text = formatTweet(data);

    console.log(tweet_text);

    if (data.tokens > 0) {
        const processedImage = await getBase64(data.image);

        twitterClient.post('media/upload', { media_data: processedImage }, (error, media, response) => {
            if (!error) {
                const tweet = {
                    status: tweet_text,
                    media_ids: [media.media_id_string]
                };
                sendTextTweet(tweet);
            } else {
                console.log(error);
                sendTextTweet({
                    status: tweet_text
                });
            }
        });
    } else {
        sendTextTweet({
            status: tweet_text
        });
    }

}

async function tweetMint(data) {
    const tweet_text = formatTweet(data, true);

    // console.log(tweet_text);

    if (data.tokens.length > 0 && process.env.IPFS_IMAGE_CID) {
        let media_ids = new Array();

        for (let i = 0; i < data.tokens.length; ++i) {
            const processedImage = await getBase64(`https://ipfs.io/ipfs/${process.env.IPFS_IMAGE_CID}/${data.tokens[i]}.png`);

            twitterClient.post(
                'media/upload',
                {
                    media_data: processedImage
                },
                (error, media, response) => {
                    if (!error) {
                        media_ids.push(media.media_id_string);

                        if (i == (data.tokens.length - 1)) {
                            sendMediaTweet(tweet_text, media_ids);
                        }
                    } else {
                        console.log(error)
                    }
                }
            );

        }
    }
}

function sendTextTweet(tweet_text)
{
    twitterClient.post(
        'statuses/update',
        tweet_text,
        (error, tweet, response) => {
            if (!error) {
                console.log(`Successfully tweeted: ${tweet_text.status}`);
            } else {
                console.error(error);
            }
        }
    );
}

function sendMediaTweet(tweet_text, media_ids)
{
    let meta_params = {media_id: media_ids[0]};

    twitterClient.post('media/metadata/create', meta_params, function (error, data, response) {
        if (!error) {
            let params = { status: tweet_text, media_ids: media_ids};
            twitterClient.post('statuses/update', params, function (error, data, response) {
                if (error) {
                    console.log(`Error occured updating status\t${error}`);
                } else {
                    console.log(`Successfully tweeted: ${tweet_text.status}`);
                }
            });
        } else {
            console.log(`Error creating metadata\t${error}`);
            process.exit(-1);
        }
    });
}

function formatTweet(data, mint= false) {
    let tweet = mint ? process.env.MINT_TWEET_TEMPLATE : process.env.SALE_TWEET_TEMPLATE;

    const keys = [
        'PROJECT_NAME',
        'PROJECT_TWITTER',
        'TOKEN_ID',
        'SALE_PRICE',
        'SALE_CURRENCY',
        'MARKETPLACE_NAME',
        'MARKETPLACE_LINK',
        'DAPP_URL',
        "TOKEN_IDS"
    ];

    const vars = [
        data.project.name,
        data.project.twitter,
        data.sale.token ?? '',
        data.sale.price,
        data.sale.currency.symbol ?? data.sale.currency.name,
        data.marketplace.twitter_handle ? `@${data.marketplace.twitter_handle}` : data.marketplace.name,
        `${data.marketplace.site}${process.env.PROJECT_CONTRACT_ADDRESS}/${data.sale.token}`,
        process.env.DAPP_URL,
        `#${data.tokens.join(', #')}`
    ];

    for (i in vars) {
        if (keys[i] && keys[i].length > 0){
            tweet = tweet.replace(`[${keys[i]}]`, vars[i]);
        }
    }

    return tweet;
}

// Format a provided URL into it's base64 representation
async function getBase64(url) {
    return axios.get(url, { responseType: 'arraybuffer'}).then(response => Buffer.from(response.data, 'binary').toString('base64'))
}

module.exports = {
    tweet: tweet,
    tweetMint: tweetMint
};
