# DegenHouse NFT Sales/Mints Twitter Bot

## Setup

### abi.json

- Retrieve the "Contract ABI" for your contract from PolygonScan.com. Copy the JSON and paste it into the ./abi.json file in the root.

### Alchemy

- Create an account at [Alchemy.com](https://alchemy.com) & create a new application on Polygon Mainnet. Once you've created a project, you should be able to grab the API keys and add these to the .env file.

### Twitter

- Request a [Twitter Developer Account](https://developer.twitter.com/en/apply-for-access) (with [Elevated Access](https://developer.twitter.com/en/portal/products/elevated), then create a Twitter Developer App (make sure you change it to have both read/write permissions)

- Make sure you are logged in to the Twitter account you want the bot to run on (as the next step will be authorizing the bot to post on your account)

### Heroku

Create a new Heroku account + app & set the project as a remote branch of your git repo (see [Heroku Remote](https://devcenter.heroku.com/articles/git#creating-a-heroku-remote))

In the Settings section of your Heroku app you'll see a **Config Vars** section. Add the following config vars:

- PROJECT_NAME - The name of the project
- PROJECT_CONTRACT_ADDRESS - The contract address you want to monitor sales for
- PROJECT_WEBSITE - The website for your project
- PROJECT_TWITTER_HANDLE - your projects twitter handle (without the @)
- TWEET_TEMPLATE - Template to use for posting to twitter (Please see the below vars that can be used)
- ALCHEMY_WEBSOCKET_URL -
- CONSUMER_KEY - Your Twitter Developer App's Consumer Key
- CONSUMER_SECRET - Your Twitter Developer App's Consumer Secret
- ACCESS_TOKEN_KEY - The Access Token Key of the Twitter Account your bot is posting from
- ACCESS_TOKEN_SECRET - The Access Token Secret of the Twitter Account your bot is posting from
- IPFS_IMAGE_CID - The CID from ipfs for the images, this will auto pull the image and ad it to the post

Now you're ready to release - just push up the code via. git to the Heroku remote (see [Heroku Remote](https://devcenter.heroku.com/articles/git#creating-a-heroku-remote) if you need help doing this).

Make sure you are using `worker` dynos and not `web` dynos - you can set this in the CLI your project with:

```sh
heroku ps:scale web=0
heroku ps:scale worker=1
```

### Twitter Template Vars

Here is a list of variable you can use when creating your twitter post, please make sure this is set prior to deploying

- PROJECT_NAME - This wil add the full name of the project to the tweet
- PROJECT_TWITTER - This will tag the projects main twitter account in the tweet
- TOKEN_ID - This will add the token ID fo the sold NFT to the tweet
- SALE_PRICE - This will show the sale price
- SALE_CURRENCY - This shows the currency used for the sale
- MARKETPLACE_NAME - this tags the marketplace the sale was on
- MARKETPLACE_LINK - This adds a direct link to the NFT on the platform it was sold on

Get stuck reach out on Twitter (@0xDegenHouse) or Discord (https://discord.gg/FQAA9ARE)
