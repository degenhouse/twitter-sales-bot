// external
const { createAlchemyWeb3 } = require('@alch/alchemy-web3');
const axios = require('axios');
const { ethers } = require('ethers');
const retry = require('async-retry');
const _ = require('lodash');

// local
const { settings } = require('./settings.js');
const { tweet, tweetMint } = require('./tweet');

// connect to Alchemy websocket
const web3 = createAlchemyWeb3(settings.alchemy.websocket);

// sometimes web3.js can return duplicate transactions in a split second, so
let lastTransactionHash;

async function monitorContract() {

  const contract = new web3.eth.Contract(
      settings.contract.abi,
      settings.contract.address
  );

  contract.events
    .Transfer({})
    .on('connected', (subscription_id) => {
      console.log(`Connected: ${subscription_id}`);
    })
    .on('data', async (data) => {
      await processEvent(data);
    })
    .on('error', (error, receipt) => {
      // if the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
      console.error(error);
      console.error(receipt);
    });
}

async function processEvent(event) {
  const transaction_hash = event.transactionHash.toLowerCase();
  let transaction_type = '';

  if (transaction_hash == lastTransactionHash) {
    return;
  }

  lastTransactionHash = transaction_hash;

  const receipt = await retry(
      async (bail) => {
        const rec = await web3.eth.getTransactionReceipt(transaction_hash);
        if (rec == null) {
          throw new Error('receipt not found, try again');
        }
        return rec;
      },
      {
        retries: 5,
      }
  );

  const recipient = receipt.to.toLowerCase();

  // not a marketplace transaction transfer, skip
  if (!(recipient in settings.marketplaces)) {
    if (recipient == process.env.PROJECT_CONTRACT_ADDRESS.toLowerCase()) {
      return await processMintEvent(event, receipt);
    }
    return;
  }

  const market = _.get(settings.marketplaces, recipient);

  let currency = { // default to ETH
    name: ' Matic',
    decimals: 18,
    threshold: 1,
  };

  let tokens = [];
  let totalPrice;
  //
  // console.log('----------------------------------------')
  for (let log of receipt.logs) {
    const log_address = log.address.toLowerCase();

    if (log_address in settings.currencies) {
      currency = settings.currencies[log_address];
    }

    // token(s) part of the transaction
    if (log.data == '0x' && settings.events.transfer_event_types.includes(log.topics[0])) {
      const tokenId = web3.utils.hexToNumberString(log.topics[3]);
      tokens.push(tokenId);
    }

    // transaction log - decode log in correct format depending on market & retrieve price

    let decoded_log_data = null;

    if (settings.events.sale_event_types.includes(log.topics[0])) {

      let decoded_log_data = [];

      try {
        decoded_log_data = web3.eth.abi.decodeLog(
            market.log_decoder,
            log.data
        );

        if (decoded_log_data.price) {
          totalPrice = ethers.utils.formatUnits(
              decoded_log_data.price,
              currency.decimals
          );
        }
      } catch (e) {
        console.log(e);
      };
    }
  }

  if (tokens.length > 0) {
    let image = '';
    let rarity = '';
    let metadata = [];

    await tweet(
        {
          'project': {
            'name': process.env.PROJECT_NAME,
            'twitter': process.env.PROJECT_TWITTER_HANDLE,
            'website': process.env.PROJECT_WEBSITE,
          },
          'sale': {
            'token': tokens[0],
            'price': totalPrice,
            'currency': currency
          },
          'marketplace': market,
          'link': market.site ? `${market.site}${process.env.CONTRACT_ADDRESS}/${tokens[0]}`:'',
          'image': process.env.IPFS_IMAGE_CID ? `https://ipfs.io/ipfs/${process.env.IPFS_IMAGE_CID}/${tokens[0]}.png` : false
        }
    );
  }
}

async function processMintEvent(event, receipt) {
  const recipient = receipt.to.toLowerCase();
  let tokens = [];
  let totalPrice = 0;

  let currency = { // default to ETH
    name: ' Matic',
    decimals: 18,
    threshold: 1,
  };

  for (let log of receipt.logs) {
    const log_address = log.address.toLowerCase();

    // token(s) part of the transaction
    if (log.data == '0x' && settings.events.transfer_event_types.includes(log.topics[0])) {
      const tokenId = web3.utils.hexToNumberString(log.topics[3]);
      tokens.push(tokenId);
    }

    // transaction log - decode log in correct format depending on market & retrieve price

    let decoded_log_data = null;

    if (settings.events.mint_event_types.includes(log.topics[0])) {

      let decoded_log_data = [];

      try {
        decoded_log_data = web3.eth.abi.decodeLog(
            [
              {
                type: 'uint',
                name: 'price'
              }
            ],
            log.data
        );

        if (decoded_log_data.price) {
          totalPrice = ethers.utils.formatUnits(
              decoded_log_data.price,
              currency.decimals
          );
        }
      } catch (e) {
        console.log(e);
      };
    }
  }

  if (totalPrice > 0 && tokens.length > 0) {
    await tweetMint(
        {
          'project': {
            'name': process.env.PROJECT_NAME,
            'twitter': process.env.PROJECT_TWITTER_HANDLE,
            'website': process.env.PROJECT_WEBSITE,
          },
          'sale': {
            'token': tokens[0],
            'price': totalPrice,
            'currency': currency
          },
          'tokens': tokens,
          'marketplace': '',
          'link': '',
          'image': process.env.IPFS_IMAGE_CID ? `https://ipfs.io/ipfs/${process.env.IPFS_IMAGE_CID}/${tokens[0]}.png` : false
        }
    );
  }
}

monitorContract();
